const express=require("express")
const router=express.Router()


const auth=require("../auth.js")
const orderController=require("../controllers/orderController.js")



// auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.post("/users/checkout", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) 
    orderController.addOrder(req.body, userData).then(resultFromController => res.send(resultFromController))
})



// retrieve all active orders
router.get("/users/orders", (req, res) => {
	orderController.getOrder().then(resultFromController => res.send(resultFromController))
})


// auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.get("/users/myOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) 
    orderController.retrieveAuthenticatedOrder(req.body, userData).then(resultFromController => res.send(resultFromController))
})



module.exports=router