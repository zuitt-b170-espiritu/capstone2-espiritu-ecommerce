const express=require("express")
const router=express.Router()


const auth=require("../auth.js")
const productController=require("../controllers/productController.js")



// auth.verify - ensures that a user is logged in before proceeding to the next part of the code; this is the verify function inside the auth.js
router.post("/log", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) 
    productController.addProduct(req.body, userData).then(resultFromController => res.send(resultFromController))
})


router.get("/allproducts", (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController))
})


// retrieve all active product
router.get("/products", (req, res) => {
	productController.getActive().then(resultFromController => res.send(resultFromController))
})


// retrieve a product
router.get("/products/:productId",  (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params.productId).then(result => res.send(result))
})


// update a product
router.put("/products/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(result => res.send(result))
})


// archive a product
router.put("/products/:productId/archive", auth.verify, (req, res) => {
	productController.archivedProduct(req.params, req.body).then(result => res.send(result))
})


// retrieve a product
router.get("/price/:productId",  (req, res) => {
	console.log(req.params.price);
	productController.getPrice(req.params.productId).then(result => res.send(result))
})

module.exports=router