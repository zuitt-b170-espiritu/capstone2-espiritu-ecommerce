const express=require("express")
const router=express.Router()


const auth=require("../auth.js")
const userController=require("../controllers/userController.js")


// user registration
router.post("/register", ( req, res ) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// user login
router.post("/login", ( req, res ) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
})


router.get("/getuser", ( req, res ) => {
	userController.getAllUser(req.body).then(resultFromController => res.send(resultFromController))
})


router.get("/details", auth.verify, (req, res)=> {
	
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	userController.getProfile( {userId: userData.id} ).then(resultFromController => res.send(resultFromController))
} )


// update user
router.put("/update/:userId", auth.verify, (req, res) => {
	userController.updateUser(req.params, req.body).then(result => res.send(result))
})

module.exports=router