// set up dependencies
const User=require("../models/user.js")
const Product=require("../models/product.js")
const auth=require("../auth.js")
const bcrypt=require("bcrypt") //used to encrypt user password

module.exports.checkEmail=(requestBody)=>{
	return User.find({email:requestBody.email}).then(result=>{
		if(error){
			console.log(error)
			return false
		}else{
			if(result.length>0){
				// return result
				return true
			}else{
				// return res.send(email does not exist)
				return false
			}
		}
	})
}

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		/*firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,*/
		email: reqBody.email,
		
		password: bcrypt.hashSync(reqBody.password, 10),
		
	})
	return newUser.save().then((saved, error) =>{
		if (error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}


// retrieve all user
module.exports.getAllUser = () => {
	return User.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}

module.exports.userLogin = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result => {
		if (result === null) {
			console.log("Please log in first");
			return false
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
				
			} else {
				return false
			}
		}
	})
}



// update a user
module.exports.updateUser = ( reqParams, reqBody ) => {
	let updatedUsers = {
		email: reqBody.email,
		// password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin:reqBody.isAdmin
	}
	
	return User.findByIdAndUpdate(reqParams.userId, updatedUsers).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

