const Product = require("../models/product")
const User = require("../models/user")

module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin === false) {
            return "You are not an admin"
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
                // isActive:reqBody.isActive
            })
        
            //Saves the created object to the database
            return newProduct.save().then((product, error) => {
                //Product creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Product creation successful"
                }
            })
        }
        
    });    
}

// retrieve all products
module.exports.getAllProduct = () => {
	return Product.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}



module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result
	})
}

// retrieve all active products
module.exports.getActive = () => {
	return Product.find( { isActive: true } ).then((result,error) => {
		if (error) {
			console.log(error)
		}else{
			return result
		}
	})
}

// update a product
module.exports.updateProduct = ( reqParams, reqBody ) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive,
		createdOn: reqBody.createdOn
	}
	
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}



// archiving a product
module.exports.archivedProduct=(reqParams, reqBody)=>{
	let updatedProduct = {
		isActive: reqBody.isActive
	} 

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error)=>{
			if (error) {
				return false
			}else{
				return true
			}
	})
}



module.exports.getPrice = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}