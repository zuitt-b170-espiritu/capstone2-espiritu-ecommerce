const Product = require("../models/product")
const User = require("../models/user")
const Order = require("../models/order")


module.exports.retrieveAuthenticatedOrder = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin === false) {
            return "You are not an admin"
        } else {
            let newOrder = new Order({
                totalAmount: reqBody.totalAmount,
                purchaseOn: reqBody.purchaseOn,
               
            })
        
            return Order.find( ).then((result,error) => {
                if (error) {
                    console.log(error)
                }else{
                    return result
                }
            })
        }
        
    });    
}


/*module.exports.checkOut = (reqBody) => {
    return User.findOne( { email: reqBody.email } ).then(result => {
        if (result === null) {
            console.log("Please log in first");
            return false
        } else {
            
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if (isPasswordCorrect) {
                return {access: auth.createAccessToken(result.toObject())}

                let newOrder = new Order({
                totalAmount: reqBody.totalAmount,
                purchaseOn: reqBody.purchaseOn,
                
            } else {
                return false
            }
        }
    })
}


*/



// retrieve all orders
module.exports.getOrder = () => {
    return Order.find( ).then((result,error) => {
        if (error) {
            console.log(error)
        }else{
            return result
        }
    })
}