const mongoose=require("mongoose")

const userSchema=new mongoose.Schema({
	/*firstName:{
		type:String,
		required:[true,"First Name is required"]
	},
	lastName:{
		type:String,
		required:[true,"Last Name is required"]
	},
	age:{
		type:String,
		required:[true,"Age is required"]
	},
	gender:{
		type:String,
		required:[true,"Gender is required"]
	},*/
	email:{
		type:String,
		required:[true,"Email is required"]
	},
	password:{
		type:String,
		required:[true,"Password is required"]
	},
	/*mobileNo:{
		type:String,
		required:[true,"Mobile number is required"]
	},*/
	isAdmin:{
		type:Boolean,
		default:false
	},

	/*product:[
	{
		productId:{
			type:String,
			required:[true,"Product ID is required"]
		},
	
		productDesc:{
			type:String,
			required:[true,"Product description is required"]
		},
		price:{
			type:number,
			required:[true,"Product price is required"]
		}
	}
	]*/

})


module.exports=mongoose.model("User",userSchema);