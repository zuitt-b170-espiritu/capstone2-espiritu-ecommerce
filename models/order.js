const mongoose=require("mongoose")


const orderSchema=new mongoose.Schema({
	totalAmount:{
		type:Number,
		required:[true, "Total amount is required"]
	},
	
	purchaseOn:{
		type:Date,
		default:new Date()
	},

	orderProd:[
		{
			productId:{
				type:String,
				required:[true,"Product ID is required"]
			},
		
			description:{
				type:String,
				required:[true,"Description is required"]
			},

			userId:{
				type:String,
				required:[true,"User ID is required"]
			},

		}
		]
	})



module.exports=mongoose.model("Order", orderSchema)